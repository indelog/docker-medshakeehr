FROM php:7.4-fpm

LABEL version="1.0"
LABEL description="Non official image for MedShakeEHR-base"
LABEL author="DEMAREST Maxime (Indelog) <contact@indelog.fr>"

# Step 0.0 : install php deps

RUN set -ex ;\
  savedAptMark="$(apt-mark showmanual)" ;\
  apt-get update ;\
  apt-get install -y --no-install-recommends \
    libc-client-dev \
    libkrb5-dev \
    libcurl4-openssl-dev \
    libxml2-dev \
    libmagickwand-dev \
    libzip-dev \
    libonig-dev \
    libgpgme-dev \
  ;\
  rm -rf /var/lib/apt/lists/* ;\
  PHP_OPENSSL=yes docker-php-ext-configure imap --with-kerberos --with-imap-ssl ;\
  docker-php-ext-install \
    gd \
    intl \
    curl \
    zip \
    xml \
    imap \
    mysqli \
    soap \
    dom \
    mbstring \
  ;\
  pecl install imagick && docker-php-ext-enable imagick ;\
  pecl install gnupg && docker-php-ext-enable gnupg ;\
  # Only used in debug mode \
  pecl install xdebug ;\
  apt-mark auto '.*' ;\
  apt-mark manual $savedAptMark; \
  ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
    | awk '/=>/ { print $3 }' \
    | sort -u \
    | xargs -r dpkg-query -S \
    | cut -d: -f1 \
    | sort -u \
    | xargs -rt apt-mark manual; \
  apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
  rm -rf /var/lib/apt/lists/* ;\
  rm -rf /var/cache/apt/*

### Step 0.1 : install basics utilities required by MedShakeEHR

RUN set -ex ;\
  apt-get update ;\
  # Necessary for install openjdk-11-jre-headless which is a dependence of pdftk (else we got dpkg error)
  mkdir /usr/share/man/man1/ ;\
  apt-get install -y --no-install-recommends \
    dcmtk \
    ghostscript \
    git \
    imagemagick \
    mariadb-client \
    orthanc \
    pdftk \
    sudo \
    unzip \
  ;\
  rm -rf /var/lib/apt/lists/* ;\
  rm -rf /var/cache/apt/*

### Step 0.2 : Get PHP composer

RUN set -ex ;\
  EXPECTED_COMPOSERHASH=$(curl -sL https://composer.github.io/installer.sig) ;\
  curl -sS https://getcomposer.org/installer -o ./composer-setup.php ;\
  echo "${EXPECTED_COMPOSERHASH} composer-setup.php" | sha384sum --check - ;\
  php ./composer-setup.php --install-dir=/usr/local/bin --filename=composer

### Step 1.0 : get MedShakeEHR base

ENV MEDSHAKE_BASE_SRC https://github.com/MedShake/MedShakeEHR-base/archive/
ENV MEDSHAKE_BASE_VERSION v6.0.0
ENV MEDSHAKE_BASE_SHA256SUM 7a2bd0567460f61071d72637a323d231baa4ab2db325cea9bc9b0a9c2b6e58eb

RUN set -ex ;\
  cd /var/www/ ;\
  # Get base
  curl -sSL "${MEDSHAKE_BASE_SRC}${MEDSHAKE_BASE_VERSION}.tar.gz" -o "MedShakeEHR-base.${MEDSHAKE_BASE_VERSION}.tar.gz" ;\
  echo "${MEDSHAKE_BASE_SHA256SUM} MedShakeEHR-base.${MEDSHAKE_BASE_VERSION}.tar.gz" | sha256sum --check - ;\
  # Extract files
  tar -xf "./MedShakeEHR-base.${MEDSHAKE_BASE_VERSION}.tar.gz" --strip-components=1 -C ./html/ ;\
  rm "./MedShakeEHR-base.${MEDSHAKE_BASE_VERSION}.tar.gz"

### Step 1.1 : finish medshake base install

RUN set -ex ;\
  cd /var/www/html/ ; /usr/local/bin/composer install --no-cache -n ;\
  cd /var/www/html/public_html/ ; /usr/local/bin/composer install --no-cache -n

### Step 1.2 Finalize installation

RUN chown -R www-data:www-data /var/www/html/ ;\
    # Instead use alternative install script for this image \
    rm /var/www/html/public_html/install.php ;\
    # This is required for enable apicrypt2 (attempt to fix apicrypt bug) \
    sed -i 's/^\(CipherString = DEFAULT@SECLEVEL=\)2$/\11/g' /etc/ssl/openssl.cnf  ;\
    # Set php.ini for production \
    cp -a "/usr/local/etc/php/php.ini-production" "/usr/local/etc/php/php.ini"

### Final step

COPY ./zz-medshakeehr.conf /usr/local/etc/php-fpm.d/
COPY ./entrypoint.sh /entrypoint.sh
COPY ./setup.php /setup.php
ENV MEDSHAKEEHRPATH /var/www/html/
WORKDIR /var/www/html/public_html/
# Share file with web server container
# Fix it : problem with mount overlayfs
#VOLUME ["/var/www/html/public_html"]
EXPOSE 9000/tcp
ENTRYPOINT ["/entrypoint.sh"]
