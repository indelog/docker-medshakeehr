#!/bin/sh

# Echo function
echo_err() {
   echo "\033[0;31m[ERR] : $1\033[0m"
}
echo_warn() {
    echo "\033[0;31m[WAR] : $1\033[0m"
}
echo_info() {
    echo "[INF] : $1"
}
echo_ok() {
    echo "\033[0;32m[OK] : $1\033[0m"
}

# For debug this script
[ "${DEV_ENV}" = 'yes' ] && set -x

# Configure container for developement
if [  -f /configured ] ; then
    echo_info "Container is already configured, just run php-fpm."
else
    if [ -n ${HOST_USER_ID} ] ; then
        [ -z ${HOST_USER_GID} ] && HOST_USER_GID=${HOST_USER_ID}
        echo_info "Set www-data user to $HOST_USER_ID id and $HOST_USER_ID gid." 
        usermod -u ${HOST_USER_ID} www-data
        groupmod -g ${HOST_USER_GID} www-data
    fi
    if [ "${DEV_ENV}" = 'yes' ] ; then
        # User right to permit user to run container to modify file
        echo_info "Setup container for dev environment." 
        cd "${MEDSHAKEEHRPATH}"
        # Always update composer in dev mode
        echo_info "Update composer." 
        sudo -Eu www-data  composer --no-cache -n update
        sudo -Eu www-data composer --no-cache -n update -d ./public_html/ 
        # set php ini file for devloppement
        cp -va "/usr/local/etc/php/php.ini-development" "/usr/local/etc/php/php.ini"
        # Enable xdebug
        echo_info "Enable xdebug." 
        docker-php-ext-enable xdebug
        cat << EOF >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
xdebug.force_display_errors = 1
xdebug.default_enable = 1
xdebug.halt_level = 0
xdebug.auto_trace = On
html_errors=1
EOF
        echo_ok "Dev env is set."
    else
        echo_info "Setup container for production environment."
    fi
    # Setup Medshake
    sudo -Eu www-data php -f /setup.php
    # Setup Apicryp if binary are found
    if [ -f ./apicrypt/bin/compagnonLinux-2020-01-11.run ] ; then
        [ -e ./apicrypt/bin/compagnonLinux-2020-01-11.run ] || chmod a+x ./apicrypt/bin/compagnonLinux-2020-01-11.run
        # TODO Catch error
        echo oui | ./apicrypt/bin/compagnonLinux-2020-01-11.run > /dev/null
        echo_info "Apicrypt found and installed."
    fi
    # File for indicate this container is already configured
    touch /configured
    # Run path for php-fpm
    cd "${MEDSHAKEEHRPATH}/public_html/"
fi

# Start Apicrypt companion if it found
if [ -f /etc/init.d/compagnonApicrypt ] ; then
    # TODO Catch error
    /etc/init.d/compagnonApicrypt start
    echo_info "Apicrypt companion found and started."
fi

exec /usr/local/sbin/php-fpm ${@}
