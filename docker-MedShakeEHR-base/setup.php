<?php
/*
 * TODO: Licence and Copyright
 */

// error mesg
function echo_err($mesg)
{
    echo "\033[0;31m".'[ERR] : '.$mesg."\033[0m\n";
}
// warn mesg
function echo_warn($mesg)
{
    echo "\033[0;33m".'[WAR] : '.$mesg."\033[0m\n";
}
// of mesg
function echo_ok($mesg)
{
    echo "\033[0;32m".'[OK] : '.$mesg."\033[0m\n";
}
// info mesg
function echo_info($mesg)
{
    echo '[INF] : '.$mesg."\n";
}

ini_set('display_errors', 1);
setlocale(LC_ALL, "fr_FR.UTF-8");

$homepath = getenv("MEDSHAKEEHRPATH");
$webdir = $homepath.'public_html/';

// If image run is dev mode do better check
if (getenv('DEV_ENV') == 'yes')
{
    if (empty($homepath))
    {
        echo_err('ENV MEDSHAKEEHRPATH is not set.');
        exit(64);
    }
    echo_info('ENV MEDSHAKEEHRPATH is : '.$homepath.'.');
    if (!chdir($homepath))
    {
        echo_err('Can\'t chdir in MEDSHAKEEHRPATH.');
        exit(65);
    }
}

require $homepath.'vendor/autoload.php';

// Class medshakeEHR auto-upload
spl_autoload_register(function ($class) {
    global $homepath;
    if (is_file($homepath.'/class/' . $class . '.php')) {
        include $homepath.'/class/' . $class . '.php';
    }
});

// Create config file if don't exist or is empty
if (!is_file($homepath.'config/config.yml') || filesize($homepath.'config/config.yml') == 0) {
    echo_info('Config file not exitst, create it.');
    $conf=array(
        'protocol'=>'https://',
        'host'=>(getenv('MEDSHAKEEHR_HOST') ? preg_replace('/\'|\"/', '', getenv('MEDSHAKEEHR_HOST')).':' : 'localhost:').(preg_replace('/\'|\"/', '', getenv('MEDSHAKEEHR_PORT')) ? getenv('MEDSHAKEEHR_PORT') : '8000' ),
        'urlHostSuffixe'=>'',
        'webDirectory'=>$webdir,
        'stockageLocation'=>getenv('MEDSHAKEEHR_STORAGELOCATION') ? preg_replace('/\'|\"/', '', getenv('MEDSHAKEEHR_STORAGELOCATION')) : $homepath.'stockage/',
        'backupLocation'=>getenv('MEDSHAKEEHR_BACKUPLOCATION') ? preg_replace('/\'|\"/', '', getenv('MEDSHAKEEHR_BACKUPLOCATION')) : $homepath.'backups/',
        'workingDirectory'=>$webdir.'workingDirectory/',
        'cookieDomain'=>getenv('MEDSHAKEEHR_HOST') ? preg_replace('/\'|\"/', '', getenv('MEDSHAKEEHR_HOST')) : 'localhost',
        'cookieDuration'=>31104000,
        'fingerprint'=>preg_replace('#[=/|+]#', '', base64_encode(random_bytes(8))),
        'sqlServeur'=>'db',
        'sqlBase'=>preg_replace('/\'|\"/', '', getenv('MYSQL_DATABASE')),
        'sqlUser'=>preg_replace('/\'|\"/', '',getenv('MYSQL_USER')),
        'sqlPass'=>preg_replace('/\'|\"/', '',getenv('MYSQL_PASSWORD')),
        'sqlVarPassword'=>preg_replace('#[=/|+]#', '', base64_encode(random_bytes(8))),
        'templatesFolder'=>$homepath.'templates/',
        'twigEnvironnementCache'=>false,
        'twigEnvironnementAutoescape'=>false
    );
    if (file_put_contents($homepath.'config/config.yml', Spyc::YAMLDump($conf, false, 0, true)))
    {
        echo_ok('Config file created whit this content : ');
        echo file_get_contents('./config/config.yml');
    }
    else
    {
        echo_err('Faild to create config file.');
        exit(66);
    }
}
else
{
    echo_info('Config file exists.');
}

// Load config
$p['config']=Spyc::YAMLLoad($homepath.'config/config.yml');

// Setup some dir
if (!is_dir($p['config']['backupLocation']))
{
    if (mkdir($p['config']['backupLocation'], 0770, true))
    {
        echo_ok('Create backup directory : '.$p['config']['backupLocation']);
    }
    else
    {
        echo_err('Faild to create backup dir : '.$p['config']['backupLocation']);
        exit(71);
    }
}
if (!is_dir($p['config']['stockageLocation']))
{
    if (mkdir($p['config']['stockageLocation'], 0770, true))
    {
        echo_ok('Create storage localtion : '.$p['config']['stockagelocation']);
    }
    else
    {
        echo_err('Faild to create storage location : '.$p['config']['stockagelocation']);
        exit(72);
    }
}

// Connect database
$mysqli = msSQL::sqlConnect();

// Init database if empty
if (empty(msSQL::sql2tabSimple("SHOW TABLES")))
{
    echo_info('Database isn\'t set.');
    // TODO Set this with php function
    $res = exec('mysql -u '.escapeshellarg($p['config']['sqlUser']).' -p'.escapeshellarg($p['config']['sqlPass']).' -h'.escapeshellarg($p['config']['sqlServeur']).' --default-character-set=utf8 '.escapeshellarg($p['config']['sqlBase']).' < '.$homepath.'upgrade/base/sqlInstall.sql', $cmdout, $cmdret);
    if ($cmdret > 0)
    {
        echo_err('Failed to execute : upgrade/base/sqlinstall.sql');
        exit(81);
    }
    echo_ok('upgrade/base/sqlInstall.sql correctly executed.');
    # TODO Setup default dicom server param for orthanc docker image
    $ret = msSQL::sqlQuery("
            INSERT INTO configuration (name, level, value) VALUES
                ('mailRappelLogCampaignDirectory', 'default', '".$webdir."/mailsRappelRdvArchives/'),
                ('smsLogCampaignDirectory', 'default', '".$webdir."/smsArchives/'),
                ('apicryptCheminInbox', 'default', '".$webdir."/inbox/'),
                ('apicryptCheminArchivesInbox', 'default', '".$webdir."/inboxArchives/'),
                ('apicryptCheminFichierNC', 'default', '".$webdir."/workingDirectory/NC/'),
                ('apicryptCheminFichierC', 'default', '".$webdir."/workingDirectory/C/'),
                ('apicryptCheminVersClefs', 'default', '".$homepath."apicrypt/'),
                ('apicryptCheminVersBinaires', 'default', '".$homepath."apicrypt/bin/'),
                ('dicomWorkListDirectory', 'default', '".$webdir."/workingDirectory/'),
                ('dicomWorkingDirectory', 'default', '".$webdir."/workingDirectory/'),
                ('templatesPdfFolder', 'default', '".$homepath."templates/models4print/'),
                ('templatesCdaFolder', 'default', '".$homepath."templates/CDA/')
            ON DUPLICATE KEY UPDATE value=VALUES(value)
    ");
    if (empty($ret))
    {
        echo_warn('Faild to write base configuration in database.');
    }
    echo_ok('Setup base configuration in database.');
    echo_info('Setup modules sql file.');
    $modules=scandir($homepath.'upgrade/');
        foreach ($modules as $module) {
            if ($module!='.' and $module!='..') {
                // TODO Set this with php function
                exec('mysql -u '.escapeshellarg($p['config']['sqlUser']).' -p'.escapeshellarg($p['config']['sqlPass']).' -h'.escapeshellarg($p['config']['sqlServeur']).' --default-character-set=utf8 '.escapeshellarg($p['config']['sqlBase']).' < '.$homepath.'upgrade/'.$module.'/sqlInstall.sql', $cmdout, $cmdret);
            if ($cmdret > 0) echo_warn('Failed to execute : '.'upgrade/'.$module.'/sqlInstall.sql');
            else echo_ok('upgrade/'.$module.'/sqlInstall.sql correctly executed.');
        }
    }

}
else
{
    // TODO Automatically update database if needed
    echo_info("Database is allready setup.");
}
