# Docker pour MedShakeEHR

Projet non officiel de *dockerisation* de [MedShakeEHR](https://www.logiciel-cabinet-medical.fr/).

Le dépôt du projet se trouve ici : <https://github.com/MedShake/MedShakeEHR-base>.

Pour le moment, ce projet en est encore au stade expérimental. Les fonctionnalités et la documentation vont s'étoffer avec le temps.

## Mise en place d'une instance pour la production

Non encore fonctionnel pour le moment. Voir plus bas, la partie *L'image Docker pour MedShakeEHR-base*.

Ne pas utiliser le fichier `./test_and_dev/docker-compose.yml` pour la mise en place d'une instance pour la production, cette configuration n'est pas prévue pour.

# Mise en place d'une instance de démonstration

Il est prévu de pouvoir utiliser la configuration `./test_and_dev/docker-compose.yml` pour la mise en place d'une instance de démonstration à des fins d'essai et de présentation de l'application. Cependant la configuration actuel n'est pas encore tout à fait prévus pour cette usage, certain paramètres de débogage actuellement automatiquement activés par défaut ralentissent l'exécution de l'application et peuvent provoquer l'affichage de messages de notice et *warning* PHP. 

# Mise en place d'une instance de test et de développement pour MedShakeEHR

Le ficher `./test_and_dev/docker-compose.yml` permet la création d'une instance pleinement fonctionnelle des MedShakeEHR à des fins de tests et de développement.

*Attention cependant pour les utilisateurs de Microsoft Windows : le fichier `docker-compose.yml` utilise un volume de type overlay, je ne sais pas comment cela fonctionnera sur ce système d’exploitation, non testé.*

Le fichier met en place 4 conteneurs :

* Un pour la base de données MySQL.
* Un pour l'application MedShakeEHR elle-même.
* Un pour la mise en place du serveur d'imagerie médicale *Orthanc*.
* Un pour la mise un place du serveur web *Nginx*.

## Récupérer le code de MedShakeEHR-base et des modules nécessaires.

L'environnement de test et développement n'utilise pas le code MedShakeEHR inclu dans l'image de base. À la place, il faut récupérer les sources depuis le dépôt git de l'application. Cela permet la mise en place d'une version donnée et d'y faire directement ces modifications :

```bash
$ cd ./test_and_dev/ 
$ git clone https://github.com/MedShake/MedShakeEHR-base ./MedShakeEHR-base/
```

En ce qui concerne les modules, seul un emplacement pour accueil *medMedTher* dans `MedShakeEHR-modMedTher` existe actuellement :

```bash
$ git clone https://github.com/MedShake/MedShakeEHR-modMedTher ./MedShakeERH-modMedTher/
```

Ajouter un module supplémentaire est assez simple, il suffit de cloner le dépôt dans le dossier `./dev_and_test/` **(Attention à bien nommer le dossier du module `MedShakeEHR-XXXXX` pour que son contenu soit ignoré par le `.gitignore` qui contient `dev_and_test/MedShakeEHR-*/**`)**, puis d'ajouter dans le `docker-compose.yml` `${PWD}/MedShakeEHR-XXXXX/` dans les options de montage d'*overlayfs* du volume `medshake` (au niveau du paramètre `lowerdir` où chaque emplacement est séparé par `:`).

### Les cas *MedShake-EHR-apicrypt* et *MedShakeEHR-vitalOnline*

Ces deux composants ne sont pas librement distribués avec MedShakeEHR pour des raisons de *Copyright* (voir : <https://www.logiciel-cabinet-medical.fr/documentation-technique/mise-en-oeuvre-de-la-messagerie-securisee-apicrypt.html>). Pour en disposer [contacter l'auteur de MedShakeEHR](https://www.logiciel-cabinet-medical.fr/generalites/qui-edite-medshakeehr-et-pourquoi.html#contacter-auteur-medshakeehr-4).

Si vous disposez de ces composants, il suffit de les placer dans leurs dossiers respectifs. Le script `./docker-MedShakeEHR-base/entrypoint.sh` détectera si les binaires *Apicript* sont présents et activera automatiquement le service si c'est le cas.

## Configuration de l'environnement des conteneurs

En plus du fichier `docker-composer.yml` deux autres fichiers sont utilisés pour configurer les conteneurs via des variables d'environnement dans `./test_and_dev/env/` :

* `db` configure le conteneur pour MariaDB.
* `app` configure le conteneur pour l'application MedShakeEHR.

**Vous devez créer ces deux fichiers à partir d'une copie de leur version *sample* dans le même dossier.** Il pourront ensuite être modifiés librement.

## Première exécution

De base, le serveur web mis en place par le `docker-compose.yml` utilise le port `8000`. Si ce port est utilisé sur l'hôte local, il faudra modifier les fichier `./dev_and_test/docker-composer.yml` et `./dev_and_test/env/app` pour remplacer le port `8000`.

Pour créer et déployer son instance MedShakeEHR :

```bash
$ cd ./test_and_dev/
$ docker-compose up -d
```

La première exécution de la commande prendra du temps car il faudra construire l'image pour l'application.

Une fois fait, vous pourrez y accéder via <https://localhost:8000/> **(attention à bien préciser https car sur un port non standard le navigateur utilisera par défaut http)**.

## Mise à jour de l'image de base

En pour mettre à jours l'image de base (uniquement les script de mise en route du conteneur pas de MedShakeEHR, les fichiers de l'application elle-même provenant de `./dev_and_test/MedShakeEHR-*/` en mode dev) :

```bash
$ cd ./test_and_dev/
$ docker-compose stop app
$ docker-compose rm app
$ docker-compose build app
$ docker-compose up -d app
```

## Repartir d'une installation « neuve »

Pour repartir d'une base neuve, il faut détruire totalement les conteneurs avec leurs volumes associés et supprimer les données locales :

```bash
$ cd ./test_and_dev/
$ docker-compose down -v
$ rm -r ./localdata/*
```

## Mise à jour de la base de données en cas de changement de version de MedShakeEHR

En cas de changement de version de MedShakeERH ou de l'un de ses modules, il faudra exécuter les scripts de mise à jour de la base de données à la main ou *Repartir d'une installation « neuve »*. Il est prévu que ce mécanisme se fasse automatiquement à la recréation d'un conteneur à partir d'une image plus récente.

## Les données propres à l'instance MedShakeEHR

Les données propres à cette instance MedShakeEHR sont dans le dossier `./test_and_dev/localdata/`. Vous pouvez ajouter n'importe quelle ressource dans ce dossier, elle sera directement accessible par l'application (par exemple vos propres *template* d'impression).

## Le serveur DICOM Orthanc inclus

La documentation pour l'image Docker *Orthanc* utilisé est disponible ici : <https://book.orthanc-server.com/users/docker.html>.

Par défaut, le nom d'utilisateur et le mot de passe pour accéder au serveur d'imagerie sont : `orthanc/orthanc`. Dans la configuration MedShakeEHR pour le serveur DICOM il faudra placer dans le champ `dicomHost` la valeur `orthanc:orthanc@orthanc`.

## Particularité du mode *dev*

Au démarrage de l'application, si le conteneur vient d'être créé, celle-ci peut mettre un peu de temps avant d'être accessible (le serveur web renvoie un 502). C'est parce que en mode dev le script exécutera `composer update` avant de démarrer php-fpm.

Pour les détails, voir les logs du conteneur :

```bash
$ cd ./test_and_dev/
$ docker-compose logs app
```

[Xdebug](https://xdebug.org/) automatiquement est chargé (pour disposer par exemple d'un affichage amélioré des sorties `var_dump()` et de d'avantage de détails sur les erreurs), l'affichage HTML des erreurs et avertissements de PHP est activé. Ceci ralentit l'exécution de l'application.

Dans le conteneur de l'application, l'UID et le GID effectifs pour `www-data` peuvent être déterminés en modifiant les variables `HOST_USER_ID` et `HOST_USER_GID` dans `./dev_and_test/`. Ce qui permet de modifier les fichiers de l'application directement avec les droits de son utilisateur courant.

# L'image Docker pour MedShakeEHR-base

Le dossier `./docker-medshake/` contient les fichiers nécessaires à la construction de l'image Docker pour MedShakeEHR Base (pour connaître la version exacte intégrée dans l'image, voir les variables d'environnement `MEDSHAKE_BASE_SRC`, `MEDSHAKE_BASE_VERSION` et `MEDSHAKE_BASE_SHA256SUM` du `Dockerfile`).

Je ne sais pas encore quel serait la meilleure manière d'intégrer les autre modules MedShakeEHR. Pour le moment, j'envisage de faire une image Docker par module depuis celle de base ; ce qui serait plus simple mais ne permettrait pas d'intégrer plusieurs module dans une même image. On pourrait faire aussi des images dédiées à des utilisations spécifiques.

Pour le moment, l'image n'est pas utilisable en tant que telle. Je rencontre un problème pour la persistance des données dans un volume donné. MedShakeEHR ne dispose pas d'emplacement dédié pour y placer les données propres à une instance particulière et les fichiers des éventuels modules. Ils peuvent se trouver à divers emplacement dans le dossier racine du projet qui sont paramétrables par l'utilisateur. La solution envisagée pour résoudre ce problème est l'utilisation d'[OverlayFS](https://fr.wikipedia.org/wiki/OverlayFS) (cependant, je ne sais pas comment gérer ça sous *Microsoft Windows*, à tester). J'en propose un exemple d'implémentation dans `./dev_and_test/docker-compose.yml` mais cet exemple récupère les fichier de l'application depuis des dossier présents dans `./dev_and_test/` et non depuis d'image docker. Dans le Docker file `VOLUME ["/var/www/html/public_html"]` est commenté car pour le moment cela pose problème avec l'utilisation de volumes *overlay*.

# TODO

* Mise à jour automatique de la base de données en cas de recréation du conteneur pour l'application MedShakeEHR avec une version plus récente.
* Utilisation de `./dev_and_test/docker-composer.yml` pour la mise en place d'une démo (désactiver les fonctionnalités liées au débogage dans ce cadre).
* Inclure la possibilité de générer automatiquement un jeu de données de test via l'utilisation de [VillagePeople](https://github.com/MedShake/VillagePeople) pour les instances de démo et dev.
* Finaliser une version utilisable en production.
